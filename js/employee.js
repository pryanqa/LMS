$(document).ready(function() {

            localStorage.getItem("location") == null ? localStorage.setItem("location", $("#json-content").html()) : "";
            var store = JSON.parse(localStorage.getItem("location"));
            localStorage.getItem("referstr") == null ? localStorage.setItem("referstr", $("#refid").html()) : "";
            var refstr = JSON.parse(localStorage.getItem("referstr"));
            $('#ename').attr("value",store[refstr.cat][refstr.no].username);
            $('#emp-id').attr("value",store[refstr.cat][refstr.no].id);
            $('.username').text(store[refstr.cat][refstr.no].username);
            $('.userid').text(store[refstr.cat][refstr.no].id);
            $('#utilised').text(parseInt(store[refstr.cat][refstr.no].leaves));
            $('#left').text(24 - parseInt(store[refstr.cat][refstr.no].leaves));
            console.log(store[refstr.cat][refstr.no].leave);
            $(document).on("click", ".submit", function() {
                $('#error').text("");
                var today = new Date();
                var fromdate = new Date($('#from').val());
                var todate = new Date($('#to').val());
                var fromday = fromdate.getDay($('#from').val());
                var todday = todate.getDay($('#from').val());
                var days = todate.getDate() - fromdate.getDate();
                var gap = fromdate.getDate() - today.getDate();
                var m = today.getMonth() - fromdate.getMonth();
                var yr = today.getFullYear() - fromdate.getFullYear();
                if( todday == 6 || fromday == 6|| today == 7 || fromday == 7){
                    $('#error').text("Exclude weekend holidays");
                    return false;

                }
                gap = gap + m * 30 + yr * 365;
                if (gap < 0) {
                    $('#error').text("invalid From date");
                    return false;
                }
                if (days < 1) {
                    $('#error').text("invalid To date");
                    return false;
                }
                if (days > 3) {
                    if (gap < 30) {
                        $('#error').text("long leave should be applied before 30 days");
                        return false;
                    }
                }
                store[refstr.cat][refstr.no].leave.push({
                    "ename": store[refstr.cat][refstr.no].username,
                    "empid": store[refstr.cat][refstr.no].eid,
                    "type": $('#type').val(),
                    "from": $('#from').val(),
                    "to": $('#to').val(),
                    "days": days,
                    "msg": $('#msg').val(),
                    "approval": "",
                    "cancel": "0"
                })
                localStorage.setItem("location", JSON.stringify(store));
                store = JSON.parse(localStorage.getItem("location"));
                refstr = JSON.parse(localStorage.getItem("referstr"));
                $('#utilised').text(parseInt(store[refstr.cat][refstr.no].leaves));
                $('#left').text(24 - parseInt(store[refstr.cat][refstr.no].leaves));
            });


            for (var i = 0; i < store.employee.length; i++) {
                for (var j = 0; j < store.employee[i].leave.length; j++) {
                	if(store[refstr.cat][refstr.no].username == store.employee[i].leave[j].ename ){
                    var lstatus;
                    var lcancel;
                    if (store.employee[i].leave[j].approval == "") {
                        lstatus = "Awaiting";
                    }
                    if (store.employee[i].leave[j].approval == "0") {
                        lstatus = "declined";
                    }
                    if (store.employee[i].leave[j].approval == "1") {
                        lstatus = "approved";
                    }
                    if (store.employee[i].leave[j].cancel == "0") {
                        lcancel = '<button  id="cancel" type="button">Cancel</button>'
                    }
                    if (store.employee[i].leave[j].cancel == "1") {
                        lcancel = '<p class="text-danger">cancelled!</p>'
                    }


                    $('.status').append('<tr id="' + j + '">' +
                        '<td>' + store.employee[i].leave[j].ename + '</td>' +
                        '<td>' + store.employee[i].leave[j].type + '</td>' +
                        '<td>' + store.employee[i].leave[j].from + '</td>' +
                        '<td>' + store.employee[i].leave[j].to + '</td>' +
                        '<td>' + store.employee[i].leave[j].days + '</td>' +
                        '<td>' + lstatus + '</td>' +
                        '<td>' + lcancel + '</td>' +
                        '</tr>');
                }
            }
	        }
     
            $(document).on("click", "#cancel", function() {
                    var temp1 = $(this).parents("tr").attr("id");
                    var temp2 = $('#' + temp1).children().html();
                    for (var i = 0; i < store.employee.length; i++) {
                    for( var j=0; j < store.employee[i].leave.length ; j++){

                        if (temp2 == store.employee[i].leave[j].ename) {
                            store.employee[i].leave[parseInt(temp1)].cancel = 1;
                            store.employee[i].leaves = parseInt(store.employee[i].leaves)-parseInt(store.employee[i].leave[parseInt(temp1)].days);
                            localStorage.setItem("location", JSON.stringify(store));
                            store = JSON.parse(localStorage.getItem("location"));
                            $(this).replaceWith('<p class="text-danger">cancelled!</p>');
                            return false; 
                            }
                        }
                    }
            });
});