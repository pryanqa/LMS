$(document).ready(function () {

    var store = JSON.parse(localStorage.getItem("location"));
    var refstr = JSON.parse(localStorage.getItem("referstr"));
    $('.username').text(store[refstr.cat][refstr.no].username);
    $('.userid').text(store[refstr.cat][refstr.no].id);
     for (var i = 0; i <store.employee.length; i++){
     for (var j = 0; j <store.employee[i].leave.length; j++){
     var lstatus;  
      if(store.employee[i].leave[j].approval== ""){ lstatus = "Awaiting"; }
      if(store.employee[i].leave[j].approval== "1"){ lstatus = "approved" ;}
      if(store.employee[i].leave[j].approval== "0"){ lstatus = "declined"; }
     if(store.employee[i].leave[j].cancel == "1"){ lstatus = "cancelled" ;}
         $('.applications').append('<tr id="'+j+'">'+
              '<td>'+store.employee[i].leave[j].ename+'</td>'+
              '<td>'+store.employee[i].leave[j].empid+'</td>'+
              '<td>'+store.employee[i].leave[j].type+'</td>'+
              '<td>'+store.employee[i].leave[j].from+'</td>'+
              '<td>'+store.employee[i].leave[j].to+'</td>'+
              '<td>'+store.employee[i].leave[j].days+'</td>'+
              '<td>'+store.employee[i].leave[j].msg+'</td>'+
              '<td>'+lstatus+'</td>'+
              '<td><button  id="approve" class="" type="button">Approve</button></td>'+
              '<td><button id="decline" type="button">Decline</button></td>'+
            '</tr>' ) ;
     }
 }
 localStorage.getItem("hjson") == null ? localStorage.setItem("hjson",$("#hr").text()):"";
    var hrs = JSON.parse(localStorage.getItem("hjson"));
    for (var i = 0; i <hrs.hrdata.length ; i++) {
      $('.disp').append('<tr>'+
      '<td>'+hrs.hrdata[i].date+'</td>'+
      '<td>'+hrs.hrdata[i].impact+'</td>'+
       '</tr>' ) ;
     }
    $(document).on("click",".add",function(){
        $('#error').text("");
        if($('#imp').val()==""){
        $('#error').text(" date required");
        return false;
        }
        var today = new Date();
        var thisdate = new Date($('#imp').val());
        var gap =  thisdate.getDate() - today.getDate();
        var mgap = thisdate.getMonth() - today.getMonth();
        var yr = thisdate.getFullYear() - today.getFullYear();
        gap= gap+ mgap*30 + 365*yr;
        if(gap < 0 ){ $('#error').text("invalid event date"); return false ;}
        hrs.hrdata.push({
            "date":$('#imp').val(),
            "impact":$('#impact').val()
        })
          localStorage.setItem("hjson",JSON.stringify(hrs));
          hrs = JSON.parse(localStorage.getItem("hjson"));
          $('.disp').empty();
            for(var i = 0 ; i < hrs.hrdata.length ; i++) {
              $('.disp').append('<tr>'+
              '<td>'+hrs.hrdata[i].date+'</td>'+
              '<td>'+hrs.hrdata[i].impact+'</td>'+
              '</tr>' ) ;
          }
      });
    $(document).on("click","#approve",function(){
      var temp1= $(this).parents("tr").attr("id");
      var  temp2 = $('#'+temp1).children().html();
      console.log(temp1);
      console.log(temp2);

      for(var i = 0 ; i  < store.employee.length ; i++){
         for( var j=0; j < store.employee[i].leave.length ; j++){
                     if(temp2 == store.employee[i].leave[parseInt(temp1)].ename){
                        store.employee[i].leave[parseInt(temp1)].approval= 1;
                        store.employee[i].msg[parseInt(temp1)]="leave"+temp1+": approved";
                        if(store.employee[i].leave[parseInt(temp1)].cancel == "0"){
                        store.employee[i].leaves = parseInt(store.employee[i].leaves)+parseInt(store.employee[i].leave[parseInt(temp1)].days);}
                        localStorage.setItem("location",JSON.stringify(store));
                        store = JSON.parse(localStorage.getItem("location"));
                        console.log(store.employee[i].leave[parseInt(temp1)].approval);
                        if(store.employee[i].leave[j].approval== "1" || store.employee[i].leave[j].approval== "0")
                        { $(this).attr("disabled", true); }
                        location.reload();
                        return false;
                  }
                }
    }
    });
     $(document).on("click","#decline",function(){
      var temp1= $(this).parents("tr").attr("id");
      var  temp2 = $('#'+temp1).children().html();

      for(var i = 0 ; i  < store.employee.length ; i++){
         for( var j=0; j < store.employee[i].leave.length ; j++){

                     if(temp2 == store.employee[i].leave[parseInt(temp1)].ename){
                        store.employee[i].leave[parseInt(temp1)].approval= 0;
                        store.employee[i].msg[parseInt(temp1)]="leave"+temp1+": Declined";
                        localStorage.setItem("location",JSON.stringify(store));
                        store = JSON.parse(localStorage.getItem("location"));
                        console.log(store.employee[i].leave[parseInt(temp1)].approval);
                        $(this).attr("disabled", true);
                        location.reload();
                        return false;
                  }
                }
    }
    });
});